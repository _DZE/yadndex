-- Есть две таблицы — users и messages:
-- users
-- UID Name
-- 1. Платон Щукин
-- 2. Лера Страза
-- 3. Георгий Атласов
-- messages
-- UID msg
-- 1 – "Привет, Платон!"
-- 3 – "Срочно пришли карту."
-- 3 – "Жду на углу Невского и Тверской."
-- 1 – "Это снова я, пиши чаще"
-- Напишите SQL-запрос, результатом которого будет таблица из двух полей: «Имя пользователя» и «Общее количество сообщений».

CREATE TABLE users (
  UID int primary key,
  Name varchar(150)
  );
INSERT INTO users(UID, Name) VALUES(1, 'Платон Щукин');
INSERT INTO users(UID, Name) VALUES(2, 'Лера Страза');
INSERT INTO users(UID, Name) VALUES(3, 'Георгий Атласов');

CREATE TABLE messages (
  UID int,
  message varchar(900)
  );

INSERT INTO messages(UID, message) VALUES(1, 'Привет, Платон!');
INSERT INTO messages(UID, message) VALUES(3, 'Срочно пришли карту.');
INSERT INTO messages(UID, message) VALUES(3, 'Жду на углу Невского и Тверской.');
INSERT INTO messages(UID, message) VALUES(1, 'Это снова я, пиши чаще');


SELECT
    u.Name, COUNT(m.UID) AS msg
FROM users u
  LEFT JOIN messages m ON m.UID = u.UID
GROUP BY
    u.UID;