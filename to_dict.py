"""
Есть два списка разной длины. В первом содержатся ключи, а во втором значения.
Напишите функцию, которая создаёт из этих ключей и значений словарь.
Если ключу не хватило значения, в словаре должно быть значение None.
Значения, которым не хватило ключей, нужно игнорировать.
"""

#Наборы данных
big   = [1,2,3,4,5,6,7,8,9]
small = [1,2,3,4,5,6]

class custom_zip():
	@staticmethod
	def simple(keys, values):
		values = values[:]
		diff = len(keys) - len(values)
		if diff > 0:
			for i in range(diff):
				values.append(None)
		else:
			values = values[:diff]
		return dict(zip(keys, values))

	@staticmethod
	def _comprehensions(keys, values):
		len_keys = len(keys)
		len_values = len(values)
		return dict([(keys[i], values[i] if len_values > i else None) for i in range(len_keys)])


if __name__ == '__main__':
	a = custom_zip._comprehensions(big, small)
	b = custom_zip._comprehensions(small, big)
	print(a)
	print(b)

	a = custom_zip.simple(big, small)
	b = custom_zip.simple(small, big)
	print(a)
	print(b)

	# OUTPUT
	# {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: None, 8: None, 9: None}
	# {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6}
	# {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: None, 8: None, 9: None}
	# {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6}