"""
В системе авторизации есть ограничение: 
- логин должен начинаться с латинской буквы,
- состоять из латинских букв, цифр, точки и минуса,
- но заканчиваться только латинской буквой или цифрой;
минимальная длина логина — один символ, максимальная — 20.
Напишите код, проверяющий соответствие входной строки этому правилу.
Придумайте несколько способов решения задачи и сравните их.
"""
import re
import string
import itertools
import time

tests = ['A-']

def reCheck(testString):
	p = re.match(r'^[A-z]([\w\.\-]{0,18}[A-z\d])?$', testString)
	return bool(p)

def check(testString):
	if( len(testString) < 20 and len(testString) > 0 ):
		if( testString[0] in string.ascii_letters + string.ascii_uppercase):
			case = string.ascii_letters + string.ascii_uppercase + string.digits + '-.'
			map_obj = map(lambda x: x in case, testString[0:-1])
			if all(list(map_obj)):
				case = string.ascii_letters + string.ascii_uppercase + string.digits
				if( testString[-1] in case ):
					return True
	return False

def WTFCheck(testString):
	return all([
		len(testString) < 20 and len(testString) > 0,
		testString[0] in string.ascii_letters + string.ascii_uppercase,
		all(list(map(lambda x: x in string.ascii_letters + string.ascii_uppercase + string.digits + '-.', testString[0:-1]))),
		testString[-1] in  string.ascii_letters + string.ascii_uppercase + string.digits
		])


[char for char in list(string.printable)]
testString = 'a'

r = []

print( 'start', len(list(itertools.product(string.printable, repeat=3))))


it = itertools.product(string.printable, repeat=3)
res = 0
start = time.time()
for line in it:
	if WTFCheck( ''.join(line) ):
		res += 1
print( 'WTFCheck', time.time() - start )

it = itertools.product(string.printable, repeat=3)
start = time.time()
for line in it:
	if check( ''.join(line) ):
		res += 1
print( 'check', time.time() - start )

it = itertools.product(string.printable, repeat=2)
start = time.time()
for line in it:
	if reCheck( ''.join(line) ):
		res += 1
print( 'reCheck', time.time() - start )

# Машина - Intel i3-1205 3.1 GHz 6Gb RAM
# Тестирование конечно примитивно, но результаты таковы:
# 
# start 1000000
# WTFCheck 4.70526909828186
# check 2.6981539726257324
# reCheck 0.025002002716064453