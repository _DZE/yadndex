"""
Вы с коллегами решили написать свой Яндекс.Диск.
В качестве первоочередной задачи вы выбрали создание файловой подсистемы.
У файла есть содержимое и метаданные.
Задачей хранения содержимого занялись ваши коллеги, а вы взяли на себя организацию хранения метаданных.
Вам следует написать прототип файловой подсистемы, который работает с метаданными и умеет:
- создавать папку;
- создавать файл;
- получать информацию о папке или файле;
- получать листинг папки.
Метаданные можно хранить в любой современной БД.
"""

from tornado.ioloop import IOLoop
from tornado import gen
import motor
import functools

class DataBase():
	''' user_id - collection
		dir - document { "_id": 123, "name": "", "meta": {} "parent": id }
		Если ограничение 4мб станет проблемой можно переместить файлы в отдельные
		документы и в массиве files хранить DBRef документа с данными файла.
	'''
	def __init__(self, user_id, host='localhost', port=27017, db='TestYAD'):
		client = motor.MotorClient(host, port)
		self.collection = client[db]['u' + str(user_id)]

	@staticmethod
	def insert_callback(result, error):
		print('result', repr(result))

	@gen.coroutine
	def insert(self, document):
		result = yield self.collection.insert(document)
		return result

	@gen.coroutine
	def find_one(self, name):
		doc = yield self.collection.find_one({'name': name})
		print('doc', doc)
		if doc:
			return (True, doc)
		else:
			return (False, 'DB Insert error.')


class FileMeta():
	def __init__(self, user_id):
		# TODO: di backend
		self.db = DataBase(user_id)

	def pathValidate(self, path):
		# TODO: path validate
		pass

	def mkdir(self, path, meta, force=False):
		# TODO: platform undependent path
		# TODO: test for uniqueness
		Root = ('/',)
		path = Root + tuple(path.split('/')[1:-1])
		parent_name = path[-2]
		if not force:
			clbk = functools.partial(self.db.find_one, parent_name)
			parent = IOLoop.instance().run_sync(clbk)
			print('parent', parent)
			if parent[0]:
				doc = { 'name': path[-1], 
						'parent': parent[1]['_id'],
						'meta': { 'size': 100000,
								' date_create': 0, 'date_modif': 0}}
				clbk = functools.partial(self.db.insert, doc)
				return IOLoop.instance().run_sync(clbk)
			else:
				return (False, 'Parent not found. Maybe use force?')
		else:
			# FORCE = TRUE
			for i in path[::-1]:
				clbk = functools.partial(self.db.find_one, parent_name)
				parent = IOLoop.instance().run_sync(clbk)
				if not parent[0]:
					parent = self.mkdir('/'.join(path[:-1]), 'forse')
				else:
					parent = parent[1]
				doc = { 'name': path[-1], 
						'parent': parent,
						'meta': { 'size': 100000,
								' date_create': 0, 'date_modif': 0}}
				clbk = functools.partial(self.db.insert, doc)
				return IOLoop.instance().run_sync(clbk)


	def mkfile(self, path, name):
		pass

	def getInfo(self, path):
		pass

	def listDir(self, path):
		pass

# -----------------------------------

# meta = FileMeta(0)
# print( meta.mkdir('/dir1/', 'meta') )
# print( meta.mkdir('/dir1/dir/ddd/sdsd/123/', 'meta', force=True) )

# def y():
# 	a = yield from meta.db.find_one('/')
# 	return a
# i = y()

# print(next(i))
# print(iter(i))

def gen():
	for i in range(10):
		yield i

def x():
	a = yield from gen()
	print(a)

for xi in x():
	print(xi)
