"""
Предположим, у нас есть access.log веб-сервера.
Как с помощью стандартных консольных средств найти десять IP-адресов, от которых было больше всего запросов?
А как сделать это с помощью скрипта на Python?
"""

import random
import re
import time
import collections


# awk '{ print $3 }' a.log | uniq -c | sort -nr | head
# grep -E '([0-9]{1,3}\.){3}[0-9]{1,3}' a.log --only-matching | uniq -c | sort -nr | head


class LogCounter():

	def outPrint(self, res):
		for line in res:
			print( 'IP: {} SUM: {}'.format( line[0].ljust(16), line[1] ))

	def generateFile(self):
		f = open('access.log', 'w')
		coolLine = ''
		for line in range(5000):
			a = random.randint(1, 254)
			b = random.randint(1, 254)
			c = random.randint(1, 254)
			d = random.randint(1, 254)
			coin = random.randint(0, 1)
			if coin:
				f.write(coolLine)
			else:
				f.write( 'time in {}.{}.{}.{} comment text\n'.format(a, b, c, d))
				coolLine = 'time in {}.{}.{}.{} comment text\n'.format(a, b, c, d)
		f.write('\n\n')
		f.close()

	def fileRead(self):
		pattern = re.compile(r'(\d{1,3}\.){3}\d{1,3}')
		for line in open('access.log', 'r').readlines():
			r = pattern.search(line)
			if r:
				yield r.group(0)

	def calcDict(self):
		resDict = {}
		for ip in self.fileRead():
			resDict[ip] = resDict.get(ip, 0) + 1
		return list(sorted(resDict.items(), key=lambda x: x[1], reverse=True)[:10])

	def calcList(self):
		res = []
		for ip in self.fileRead():
				new = True
				for v in res:
					if v[0] == ip: 
						v[1] += 1
						new = False
				if new:
					res.append( [ip, 1] )
		return list(sorted( res, key=lambda item: item[1], reverse=True ))[:10]

	def calcCounter(self):
		resDict = collections.Counter(self.fileRead())
		return list(sorted(resDict.items(), key=lambda x: x[1], reverse=True)[:10])

inst = LogCounter()
inst.generateFile()

start = time.time()
inst.outPrint(inst.calcDict())
print('calcDict - {}\n'.format(time.time() - start))

start = time.time()
inst.outPrint(inst.calcCounter())
print('calcCounter - {}\n'.format(time.time() - start))

start = time.time()
inst.outPrint(inst.calcList())
print('calcList - {}\n'.format(time.time() - start))